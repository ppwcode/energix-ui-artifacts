# energix-ui-artifacts Terraform Infrastructure Definition

The infrastructure necessary for deployment of Energix UI artifacts.

## Getting started

The infrastructure is defined using [Terraform].
See [Getting started with a Terraform configuration].

[terraform]: https://peopleware.atlassian.net/wiki/x/CwAvBg
[getting started with a terraform configuration]: https://peopleware.atlassian.net/wiki/x/p4zhC
