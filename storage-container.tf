resource "azurerm_storage_container" "immutable" {
  name                  = "immutable"
  storage_account_name  = azurerm_storage_account.storage_account.name
  container_access_type = "blob"
}

resource "azurerm_storage_container" "bookmarkable" {
  name                  = "bookmarkable"
  storage_account_name  = azurerm_storage_account.storage_account.name
  container_access_type = "blob"
}
