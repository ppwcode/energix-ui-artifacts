terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.98.0"
    }
  }
}

provider "azurerm" {
  features {}

  subscription_id = "098ad191-f2fd-4240-9a7c-e269ab8394c0"
}
