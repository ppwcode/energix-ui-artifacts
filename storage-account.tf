#Create Storage account
resource "azurerm_storage_account" "storage_account" {
  name                = "energixuiartifacts"
  resource_group_name = local.resource_group_name

  location                 = "westeurope"
  account_tier             = "Standard"
  account_replication_type = "LRS"
  account_kind             = "StorageV2"

  allow_blob_public_access = true
}
