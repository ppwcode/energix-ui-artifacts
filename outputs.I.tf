output "I-ui_artifacts" {
  value = {
    id             = azurerm_storage_account.storage_account.id
    name           = azurerm_storage_account.storage_account.name
    host           = azurerm_storage_account.storage_account.primary_blob_host
    url            = azurerm_storage_account.storage_account.primary_blob_endpoint
  }
}
